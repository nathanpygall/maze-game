﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class SoundManager : MonoBehaviour
{
    public AudioClip[] generalSoundEffects;
    public AudioClip[] generalMusic;  
    public AudioClip[] ambientSoundEffects;
    public AudioClip[] transitionSoundEffects;

    static AudioSource audioSrc;
    public AudioMixer mixer;

    // Start is called before the first frame update
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playAmbient()
    {
        int soundNo = Random.Range(0,ambientSoundEffects.Length);        
        audioSrc.PlayOneShot(ambientSoundEffects[soundNo]);
    }

    public void playTransition(int roomNo)
    {
        audioSrc.PlayOneShot(transitionSoundEffects[roomNo]);
    }
    
}
