﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    idle,
    walk,
    meleeAttack,
    stagger
}

public class Enemy : MonoBehaviour
{
    public EnemyState currentState;
    public FloatValue maxHealth;
    public float health;
    public string enemyName;
    public int baseAttack;
    public float moveSpeed;
    public Transform target;
    public Animator animator;
    public SpriteRenderer spriteRenderer;


    // Start is called before the first frame update
    void Start()
    {
    }

    private void Awake()
    {
        health = maxHealth.initialValue;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void TakeDamage(float damage)
    {
        health -= damage;
        if(health<=0)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void Knock(Rigidbody2D myRidigbody, float knockTime, float damage)
    {
        StartCoroutine(KnockCo(myRidigbody, knockTime));
        TakeDamage(damage);
    }

    private IEnumerator KnockCo(Rigidbody2D myRigidbody, float knockTime)
    {
        if (myRigidbody != null)
        {
            //Debug.Log("knockco running");
            //Debug.Log(knockTime);
            yield return new WaitForSeconds(knockTime); //knocks it back for the chosen knockback time
            //Debug.Log("does this run");
            //Debug.Log("pre ve" + myRidigbody.velocity);
            myRigidbody.velocity = Vector2.zero; //stops the knockback "sliding"
            //Debug.Log("post ve" + myRidigbody.velocity);
           currentState = EnemyState.idle;
            myRigidbody.velocity = Vector2.zero;

        }
    }

    public void ChangeState(EnemyState newState)
    {
        if (currentState != newState)
        {
            currentState = newState;
        }
    }
}
