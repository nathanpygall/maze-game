﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu] //allows to create as an object using right click
public class FloatValue : ScriptableObject //this script cannot be attached to anything in the scene
{
    public float initialValue;
}
