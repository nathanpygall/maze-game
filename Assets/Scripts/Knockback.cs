﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knockback : MonoBehaviour
{
    public float knockbackPower; 
    public float knockTime;
    public float damage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("breakable") && this.gameObject.CompareTag("Player"))
        {
            other.GetComponent<Pot>().Smash();
        }

        if (other.gameObject.CompareTag("enemy") || other.gameObject.CompareTag("Player"))
        {
            Rigidbody2D hit = other.GetComponent<Rigidbody2D>(); //declaring the enemy
            if(hit != null) //if they have a rigid body 
            {
                Vector2 difference = (hit.transform.position - transform.position); //how far it wil be knocked
                difference = difference.normalized * knockbackPower; //normalised turns it into a vector1, the speed it will be knocked at
                hit.AddForce(difference, ForceMode2D.Impulse); //applies the knockback to the enemy
                if (other.gameObject.CompareTag("enemy") && other.isTrigger)
                {
                    hit.GetComponent<Enemy>().currentState = EnemyState.stagger;
                    other.GetComponent<Enemy>().Knock(hit, knockTime, damage);
                }
                if(other.gameObject.CompareTag("Player") && other.isTrigger)
                {
                    hit.GetComponent<PlayerMovement>().currentState = PlayerState.stagger;
                    other.GetComponent<PlayerMovement>().Knock(knockTime);
                }

            }
        }

        
    }

    //private IEnumerator KnockCo(Rigidbody2D enemy)
    //{
    //    if(enemy != null)
    //    {
    //        Debug.Log("knockco running");
    //        Debug.Log(knockTime);
    //        yield return new WaitForSeconds(2f); //knocks it back for the chosen knockback time
    //        Debug.Log("does this run");
    //        Debug.Log("pre ve" + enemy.velocity);
    //        enemy.velocity = Vector2.zero; //stops the knockback "sliding"
    //        Debug.Log("post ve" + enemy.velocity);
    //        enemy.GetComponent<Enemy>().currentState = EnemyState.idle;
    //    }
    //}
}
