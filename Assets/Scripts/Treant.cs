﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treant : Log
{

    private Rigidbody2D myRigidBody;


    // Start is called before the first frame update
    void Start()
    {
        currentState = EnemyState.idle;
        target = GameObject.FindWithTag("Player").transform;
        myRigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckDistance();
    }

    private void CheckDistance()
    {
        if (Vector3.Distance(target.position, transform.position) <= chaseRadius
            && Vector3.Distance(target.position, transform.position) >= attackRadius)
        {
            animator.SetBool("moving", true);
            if ((currentState == EnemyState.idle || currentState == EnemyState.walk)
                && currentState != EnemyState.stagger)
            {
                Vector3 temp = Vector3.MoveTowards(transform.position, target.position, (moveSpeed * Time.deltaTime));

                myRigidBody.MovePosition(temp);
                ChangeAnim(temp - transform.position);
                ChangeState(EnemyState.walk);
                
            }

        }
        else if (Vector3.Distance(target.position, transform.position) > chaseRadius)
        {
            animator.SetBool("moving", false);

        }
    }

    private void ChangeAnim(Vector2 direction)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            if (direction.x > 0)
            {

                SetAnimFloat(Vector2.right);
                spriteRenderer.flipX = false;

            }
            else if (direction.x < 0)
            {
                SetAnimFloat(Vector2.left);
                spriteRenderer.flipX = true;

            }
        }
        else if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
        {
            if (direction.y > 0)
            {
                SetAnimFloat(Vector2.up);
                spriteRenderer.flipX = false;

            }
            else if (direction.y < 0)
            {
                SetAnimFloat(Vector2.down);
                spriteRenderer.flipX = false;

            }
        }
    }

    private void SetAnimFloat(Vector2 setVector)
    {
        animator.SetFloat("moveX", setVector.x);
        animator.SetFloat("moveY", setVector.y);
    }
}
